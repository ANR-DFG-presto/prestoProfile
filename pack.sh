#!/bin/bash

declare -A ARCHIVES=( ["lexicon"]="Lexicon" ["trainCorpus"]="trainCorpus" )

TARGET=""
PRESET=""

function usage()
{
	cat <<-EOF
		Syntax: ${0##*/} [-p PRESET_NAME] OUTPUT_PROFILE_NAME

		Options:
		  -h | --help              	show this message
		  -p | --preset PRESET_NAME	name of the directory in ./presets/ to use to generate the profile
	EOF
}

while [ -n "${1}" ]
do
	case "${1}" in
		-p|--preset)
			shift
			PRESET="presets/${1}"
			;;
		-h|--help)
			usage
			exit 0
			;;
		*)
			TARGET="${1}"
			;;
	esac
	shift
done

if [ -z "${TARGET}" ]
then
	usage
	exit 1
fi

[ -d "${TARGET}" ] || cp -R skel "${TARGET}"
chmod -R +w "${TARGET}"

for dir in lexicon trainCorpus
do
	RESOURCE_DIR="${TARGET}/resources/${dir}"
	mkdir -p "${RESOURCE_DIR}"
	zip -9 "${RESOURCE_DIR}/${ARCHIVES[$dir]}.zip" -j "${dir}"/*.csv
done

if [ -d "${PRESET}" ]
then
	cp -R "${PRESET}"/* "${TARGET}"
fi
