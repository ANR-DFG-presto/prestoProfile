# Profil Presto

Ce dépôt contient le profil utilisé dans le cadre du projet [Presto] pour analyser le corpus à l'aide de la [chaîne de traitement](https://gitlab.com/ANR-DFG-presto/prestoProcessingLine).

Vous pouvez partir du contenu de ce dépôt pour créer des profils différents pour analyser le corpus Presto ou un autre corpus que vous aurez décidé d'analyser avec la chaîne de traitement du projet.

## Utilisation

Vous pouvez génerer un profil en exécutant dans une ligne de commande ouverte dans ce dossier

```bash
./pack.sh [--preset $NOM_D_UN_PRESET] $NOM_CHOISI_POUR_LE_PROFIL
```

Par exemple, nous avions l'habitude dans le cadre du projet Presto de générer des profils nommés «presto» ainsi:

```bash
./pack.sh presto
```

puis de déplacer le dossier ainsi généré dans le sous-dossier `profiles` de la chaîne de traitement Presto.

Le paramètre de preset est optionnel, et permet d'inclure dans le profil généré le contenu d'un des sous-répertoire existant dans le dossier *presets/*. Il attend pour valeur le nom du répertoire, pas le chemin jusqu'à lui.

## Contenu

Un profil contient deux sous-dossiers

<dl>
	<dt>config</dt>
	<dd>Un tout petit répertoire contenant seulement le fichier `sentenceBoundary.txt` dont le seul contenu doit être l'étiquette utilisée pour annoter la ponctuation qui délimite les phrases.</dd>
	<dt>resources</dt>
	<dd>Ce dossier contient l'essentiel du profil, les règles de réécriture des différentes phases de l'annotation, le corpus d'entraînement et le lexique.</dd>
</dl>

### Ressources

Si vous avez déjà entraîné TreeTagger et disposez d'un fichier de paramètres pour votre langue et votre jeu d'étiquette, vous pouvez le fournir à Presto en le plaçant dans le sous-répertoire `resources/treetagger/`.

Le lexique doit être une archive au format `zip` nommée `Lexicon.zip` et placée dans le sous-dossier `resources/lexicon/`. Cette archive doit contenir exclusivement une liste de fichiers `TSV` contenant un mot par ligne et dont les trois premières colonnes doivent contenir dans cet ordre la forme du mot `form`, le lemme correspondant `lemma` et la partie de discours correspondante `pos`.

Il en va de même pour le corpus d'entraînement, qui est une archive `zip` contenant une liste de textes annotés sous la forme de fichiers `TSV` contenant au moins une colonne `form`, une colonne `pos` et une colonne `lemma`. L'archive elle-même doit s'appeler `trainCorpus.zip` et être placée dans le sous-répertoire `resources/trainCorpus/`.

Enfin, le sous-répertoire `resources/rules` contient des règles utilisées par la chaîne de traitement pour transformer dynamiquement le flots de formes, lemmes et parties de discours contenues dans les différents fichiers ci-dessus ainsi que dans les fichiers temporaires manipulés au cours du processus d'annotation. Ils sont au nombre de 4 :

- `lexiconRules.txt`
- `postAnnotationRules.txt`
- `postTokenRules.txt`
- `trainCorpusRules.txt`

Les détails de format et de syntaxe de ces fichiers sont disponibles sur la page du [module](https://gitlab.com/ANR-DFG-presto/prestoLexicalRules) qui les applique.

## Preset

Un preset est un répertoire contenant des fichiers supplémentaires qui peuvent être inclus dans un profil, comme par exemple une liste d'expressions régulières à exclure du lexique d'entrée. Il doit exister dans le répertoire *presets/* présent à la racine de ce projet.

Le contenu du preset est importé à la toute fin de la génération du profil, ce qui implique qu'il est possible de changer un contenu existant dans le dossier *skel/* en incluant dans un preset une version différente du même fichier.
